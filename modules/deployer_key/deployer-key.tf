resource "aws_key_pair" "deployer" {
  key_name = "deployer_key"
  public_key = "${file("~/.ssh/aws_id_rsa.pub")}"
}
