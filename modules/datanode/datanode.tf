variable "ami" { description = "AMI Id" }
variable "subnet_id" { description = "Subnet id" }
variable "security_group_ids" { description = "List of security group id" }
variable "env" { description = "Environment name" }
variable "az" { description = "Availability zone" }
variable "node_id" { description = "Unique index for node 0,1,2.." }
variable "instance_type" {
  default = "m4.xlarge"
  description = "Type of EC2 instance"
}
variable "key_name" {
  default = "deployer_key"
  description = "Key to acess EC2 instances"
}
variable "delete_on_termination" {
  default = "true"
  description = "Weather to delete the block device when instance terminates"
}
variable "volume_type" {
  default = "standard"
  description = "Type of volume"
}
variable "volume_size" {
  default = "10"
  description = "Size of volume in GB"
}

resource "aws_instance" "datanode" {
  connection {
    type = "ssh"
    user = "ec2-user"
    agent = "true"
  }
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_ids}"]
  root_block_device {
    delete_on_termination = "${var.delete_on_termination}"
    volume_type = "${var.volume_type}"
    volume_size = "${var.volume_size}"
  }

  tags {
        Name = "${var.env}_datanode${var.node_id}"
  }
}

module "data_vol_11" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//ebs_volume"
  availability_zone = "${var.az}"
  device_name = "/dev/sdf"
  instance_id = "${aws_instance.datanode.id}"
}

module "data_vol_12" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//ebs_volume"
  availability_zone = "${var.az}"
  device_name = "/dev/sdg"
  instance_id = "${aws_instance.datanode.id}"
}

module "data_vol_13" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//ebs_volume"
  availability_zone = "${var.az}"
  device_name = "/dev/sdh"
  instance_id = "${aws_instance.datanode.id}"
}

resource "null_resource" "create_fs" {
  connection {
    host = "${aws_instance.datanode.public_dns}"
    type = "ssh"
    user = "ec2-user"
    private_key = "${file("~/.ssh/aws_id_rsa")}"
  }
  provisioner "file" {
    source = "${path.module}/attach_vols.sh"
    destination = "/tmp/attach_vols.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/attach_vols.sh",
      "/tmp/attach_vols.sh"
    ]
  }
}
