#!/bin/sh
COUNTER=0
for d in `lsblk -p | grep disk | awk '{print $1}'`
do
  fs=`sudo file -s $d`
  if [[ $fs == *data ]]
  then 
   sudo mkfs -t ext4 $d
  fi
  if [[ ! $(mount | grep $d) ]]
  then
    let COUNTER++
    sudo mkdir -p /c/data$COUNTER
    sudo mount $d /c/data$COUNTER
  fi
done
