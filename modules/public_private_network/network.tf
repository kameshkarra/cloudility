
variable "az" {description = "Availability zone 1"}

variable "env" {description = "Environment name"   }

variable "vpc_id" {
  description = "id of cidr for vpc"
}

variable "cidr_public_subnet" {
  description = "cidr for public subnet 1"
}

variable "cidr_private_subnet" {
  description = "cidr for private subnet 1"
}

variable "igw_id" {
  description = "id of internet gateway"
}

resource "aws_subnet" "public_subnet" {
  vpc_id = "${var.vpc_id}"
  cidr_block = "${var.cidr_public_subnet}"
  availability_zone = "${var.az}"
  map_public_ip_on_launch = "true"
  tags {
      Name = "public_subnet"
      Env  = "${var.env}"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id = "${var.vpc_id}"
  cidr_block = "${var.cidr_private_subnet}"
  availability_zone = "${var.az}"
  map_public_ip_on_launch = "false"
  tags {
      Name = "private_subnet"
      Env  = "${var.env}"
  }
}

output "public_subnet_id" {
  value = "${aws_subnet.public_subnet.id}"
}

output "private_subnet_id" {
  value = "${aws_subnet.private_subnet.id}"
}
