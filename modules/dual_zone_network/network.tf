variable "az1" {description = "Availability zone 1"}
variable "az2" {description = "Availability zone 2"}
variable "env" {description = "Environment name"   }

variable "cidr_vpc" {
  default = "10.0.0.0/16"
  description = "cidr for vpc"
}

variable "cidr_public_subnet_1" {
  default = "10.0.1.0/24"
  description = "cidr for public subnet 1"
}

variable "cidr_private_subnet_1" {
  default = "10.0.2.0/24"
  description = "cidr for private subnet 1"
}

variable "cidr_public_subnet_2" {
  default = "10.0.3.0/24"
  description = "cidr for public subnet 2"
}

variable "cidr_private_subnet_2" {
  default = "10.0.4.0/24"
  description = "cidr for private subnet 2"
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "${var.cidr_vpc}"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
  tags {
      Name = "vpc"
      Env  = "${var.env}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.mainvpc.id}"
  tags {
      Name = "internet_gateway"
      Env  = "${var.env}"
  }
  depends_on = ["aws_security_group.allow_external","aws_security_group.allow_internal"]
}

resource "aws_security_group" "allow_external" {
  name = "allow_external"
  description = "Allow selected inbound traffic"
  vpc_id = "${aws_vpc.mainvpc.id}"
  tags {
    Name = "allow_external"
    Env  = "${var.env}"
  }
}

resource "aws_security_group" "allow_internal" {
  name = "allow_internal"
  description = "Allow selected outbound traffic"
  vpc_id = "${aws_vpc.mainvpc.id}"
  tags {
    Name = "allow_internal"
    Env  = "${var.env}"
  }
}

resource "aws_security_group_rule" "allow_ssh" {
    type = "ingress"
    from_port = 22 
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.allow_external.id}"
}

resource "aws_security_group_rule" "allow_public_private_tcp" {
    type = "ingress"
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    security_group_id ="${aws_security_group.allow_internal.id}"
    source_security_group_id = "${aws_security_group.allow_external.id}"
}

resource "aws_route" "r" {
  route_table_id = "${aws_vpc.mainvpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.igw.id}"
}

module "subnet_az1" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//public_private_network"
  az = "${var.az1}"
  env = "${var.env}"
  vpc_id = "${aws_vpc.mainvpc.id}"
  igw_id = "${aws_internet_gateway.igw.id}"
  cidr_public_subnet = "${var.cidr_public_subnet_1}"
  cidr_private_subnet = "${var.cidr_private_subnet_1}"
}

module "subnet_az2" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//public_private_network"
  az = "${var.az2}"
  env = "${var.env}"
  vpc_id = "${aws_vpc.mainvpc.id}"
  igw_id = "${aws_internet_gateway.igw.id}"
  cidr_public_subnet = "${var.cidr_public_subnet_2}"
  cidr_private_subnet = "${var.cidr_private_subnet_2}"
}

output "az1_public_subnet_id" {
  value = "${module.subnet_az1.public_subnet_id}"
}
output "az1_private_subnet_id" {
  value = "${module.subnet_az1.private_subnet_id}"
}
output "az2_public_subnet_id" {
  value = "${module.subnet_az2.public_subnet_id}"
}
output "az2_private_subnet_id" {
  value = "${module.subnet_az2.private_subnet_id}"
}
output "security_group_external_id" {
  value = "${aws_security_group.allow_external.id}"
}	
output "security_group_internal_id" {
  value = "${aws_security_group.allow_internal.id}"
}
