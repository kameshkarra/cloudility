data "aws_availability_zones" "available" {
  state = "available"
}

output "az1" {
  value = "${data.aws_availability_zones.available.names[0]}"
}

output "az2" {
  value = "${data.aws_availability_zones.available.names[1]}"
}
