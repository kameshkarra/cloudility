variable "availability_zone"          { description = "Availability zone" }
variable "device_name" { description = "Name of the device being attached" }
variable "instance_id" { description = "Id of the instance the device is being attached to" }
variable "size" { 
  default = 40
  description = "Size of the volume in GB"
}

resource "aws_ebs_volume" "data_vol" {
    availability_zone = "${var.availability_zone}"
    type = "standard"
    size = "${var.size}"
}

resource "aws_volume_attachment" "ebs_attach_data_vol" {
  device_name = "${var.device_name}"
  volume_id = "${aws_ebs_volume.data_vol.id}"
  instance_id = "${var.instance_id}"
}
