variable "env" {
  default = "dev"
  description = "Environment name"
}

provider "aws" {
  region = "${file(".region")}"
}

module "az" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//availability_zones"
}

module "deployer_key" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//deployer_key"
}

module "vpc" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//dual_zone_network"
  env    = "${env}"
  az1    = "${module.az.az1}"
  az2    = "${module.az.az2}"
}

module "datanode_1" {
  source = "bitbucket.org/kameshkarra/cloudility//modules//datanode"
  ami    = "${file("hbase_ami_id.txt")}"
  subnet_id = "${module.vpc.az1_public_subnet_id}"
  security_group_ids = "${module.vpc.security_group_external_id}"
  instance_type = "t2.small"
  env = "${env}"
  az  = "${module.az.az1}"
  node_id = "1"
}
