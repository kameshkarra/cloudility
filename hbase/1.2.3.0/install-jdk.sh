#!/bin/sh
echo "Downloading jdk-8u101-linux-x64.rpm .."
wget --no-cookies --no-check-certificate --header  \
     "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie"  \
     "http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.rpm" >> install.log 2>&1

echo "Installing jdk-8u101-linux-x64.rpm .."
sudo yum localinstall jdk-8u101-linux-x64.rpm -y 
