#!/bin/sh

sudo useradd hbase

wget http://mirror.ox.ac.uk/sites/rsync.apache.org/hbase/stable/hbase-1.2.3-bin.tar.gz >> install.log 2>&1

sudo tar xf hbase-1.2.3-bin.tar.gz -C /usr/local

sudo ln -s /usr/local/hbase-1.2.3 /opt/hbase

sudo su - hbase -c "mkdir -p /home/hbase/logs"

echo "Creating environment variables"

sudo su - hbase -c "echo " " >> /home/hbase/.bashrc"
sudo su - hbase -c "echo "JAVA_HOME=/usr/java/latest" >> /home/hbase/.bashrc"
sudo su - hbase -c "echo export JAVA_HOME >> /home/hbase/.bashrc"
sudo su - hbase -c "echo "HBASE_HOME=/opt/hbase" >> /home/hbase/.bashrc"
sudo su - hbase -c "echo export HBASE_HOME >> /home/hbase/.bashrc"
sudo su - hbase -c "echo "HBASE_LOG_DIR=/home/hbase/logs" >> /home/hbase/.bashrc"
sudo su - hbase -c "echo export HBASE_LOG_DIR >> /home/hbase/.bashrc"
