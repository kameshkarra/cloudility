# 
# Create custom ami by installing hbase v1.2.3 and its dependencies
# 

# Choose a region for creating the image

provider "aws" {
  region = "us-west-2"
}

# Create a key pair to associate with the image.
# Private key should be available from the host where terraform is run

resource "aws_key_pair" "deployer" {
  key_name = "deployer-key" 
  public_key = "${file("../deployer_key.pub")}"
}

# Create the instance and install software

resource "aws_instance" "rhel-7-1-hbase-gold" {
  
  # Connection for provisioners
  connection {
    type = "ssh"
    user = "ec2-user"
    agent = "true"
  }

  ami           = "ami-4dbf9e7d"
  instance_type = "t2.small"
  key_name      = "deployer-key"

  root_block_device {
    delete_on_termination = "true"
    volume_type = "standard"
    volume_size = "6"
  }

  tags {
        Name = "rhel-7-1-hbase-gold"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /home/ec2-user/.ssh"
    ]
  }

  # Copy private key for git checkout
  provisioner "file" {
    source = "bitbucket_id_rsa"
    destination = "/home/ec2-user/.ssh/id_rsa"
  }

  # Install software
  provisioner "remote-exec" {
    inline = [
      "chmod 400 /home/ec2-user/.ssh/id_rsa",
      "ssh-keyscan bitbucket.org >> /home/ec2-user/.ssh/known_hosts",
      "sudo yum update -y",
      "sudo yum install ntp -y",
      "sudo yum install wget -y",
      "sudo yum install git -y",
      "git clone git@bitbucket.org:kameshkarra/cloudility.git",
      "chmod +x ~/cloudility/hbase/1.2.3.0/install-jdk.sh",
      "~/cloudility/hbase/1.2.3.0/install-jdk.sh",
      "chmod +x ~/cloudility/hbase/1.2.3.0/install-hadoop.sh",
      "~/cloudility/hbase/1.2.3.0/install-hadoop.sh",
      "chmod +x ~/cloudility/hbase/1.2.3.0/install-hbase.sh",
      "~/cloudility/hbase/1.2.3.0/install-hbase.sh",
      "sudo cp ~/cloudility/hbase/1.2.3.0/limits.conf /etc/security/limits.conf",
      "sudo chkconfig ntpd on"
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rhel-7-1-hbase-gold.id} >> instance_id.txt"
  }
}

resource "aws_ami_from_instance" "hbase_1_2_3_gold" {
  name = "hbase-1.2.3.0 image"
  source_instance_id = "${aws_instance.rhel-7-1-hbase-gold.id}"

  provisioner "local-exec" {
    command = "echo ${aws_ami_from_instance.hbase_1_2_3_gold.id} >> hbase_ami_id.txt"
  }

  provisioner "local-exec" {
    command = "git add .; git commit -F hbase_ami_id.txt; git push"
  }
}
