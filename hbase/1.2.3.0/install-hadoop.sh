#!/bin/sh

sudo useradd hdfs

wget http://mirror.catn.com/pub/apache/hadoop/common/stable/hadoop-2.7.3.tar.gz >> install.log 2>&1

sudo tar xf hadoop-2.7.3.tar.gz -C /usr/local
sudo ln -s /usr/local/hadoop-2.7.3 /opt/hadoop
sudo su - hdfs -c "mkdir -p /home/hdfs/logs"
sudo mkdir -p /var/lib/hadoop
sudo chown hdfs:hdfs -R /var/lib/hadoop

echo "Creating environment variables"

sudo su - hdfs -c "echo " " >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo "JAVA_HOME=/usr/java/latest" >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo export JAVA_HOME >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo "HADOOP_HOME=/opt/hadoop" >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo export HADOOP_HOME >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo "HADOOP_LOG_DIR=/home/hdfs/logs" >> /home/hdfs/.bashrc"
sudo su - hdfs -c "echo export HADOOP_LOG_DIR >> /home/hdfs/.bashrc"
